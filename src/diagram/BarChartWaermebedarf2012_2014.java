/*---------------------------------------------------
 * Hochschule f�r Technik Stuttgart
 * Fachbereich Vermessung, Informatik und Mathematik
 * Schellingstr. 24
 * D-70174 Stuttgart
 *
 * Volker Coors, 11.9.2015
 * GeoVisualisierung
 * IL3, WS 2015/16 
 * 
 * ------------------------------------------------*/

package diagram;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import data.*;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;



public class BarChartWaermebedarf2012_2014 extends Application {

	/**
	 * Erzeugt einen Datensatz f�r S�ulen- und Balkendiagramme
	 * 
	 * @param daten
	 * @return CategoryDataset
	 */
	private ObservableList<XYChart.Data<String, Number>> createXYDataSet(Datenbasis daten) {

		ObservableList<XYChart.Data<String, Number>> list = 
				FXCollections.observableArrayList();
		
		for (int i = 0; i < daten.getBeobachtungsraum().length; i++) {
			list.add(new XYChart.Data<String, Number>(
					daten.getBeobachtungsraum()[i], 
					daten.getMerkmalsauspraegungen()[i]));
		}

		return list;
	}
	
    @Override public void start(Stage stage) {

    	// Daten, die im Diagramm dargestellt werden sollen
 
    	Datenbasis data1 = new HeizwaermeBau2_2012();
    	Datenbasis data2 = new HeizwaermeBau2_2013();
    	Datenbasis data3 = new HeizwaermeBau2_2014();
    	
    	// Erstellen und Beschriften des Diagramms
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        final BarChart<String,Number> bc = 
            new BarChart<String,Number>(xAxis,yAxis);
        bc.setTitle(data1.getTopic());
        xAxis.setLabel(data1.getNameBeobachtungsraum());       
        yAxis.setLabel(data1.getNameMerkmalsauspraegung());
        
        // Zuordnung der Daten zum Diagramm
       	ObservableList<XYChart.Data<String, Number>> barChartData;
    	barChartData = createXYDataSet(data1);
        XYChart.Series<String,Number> series1 = 
        		new XYChart.Series<String,Number>(barChartData);
        series1.setName("2012");
        bc.getData().add(series1);

        barChartData = createXYDataSet(data2);
        XYChart.Series<String,Number> series2 = 
        		new XYChart.Series<String,Number>(barChartData);
        series2.setName("2013");
        bc.getData().add(series2);
        
        barChartData = createXYDataSet(data3);
        XYChart.Series<String,Number> series3 = 
        		new XYChart.Series<String,Number>(barChartData);
        series3.setName("2014");
        bc.getData().add(series3);
        
        // Rendern des Diagramms
        Scene scene  = new Scene(bc,800,600);
        stage.setTitle("GVI Beispiel: S�ulendiagramm");
        stage.setScene(scene);
        stage.show();
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
		
	}
}
