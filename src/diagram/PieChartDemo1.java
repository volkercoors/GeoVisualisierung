/*---------------------------------------------------
 * Hochschule f�r Technik Stuttgart
 * Fachbereich Vermessung, Informatik und Mathematik
 * Schellingstr. 24
 * D-70174 Stuttgart
 *
 * Volker Coors, 11.9.2015
 * GeoVisualisierung
 * IL3, WS 2015/16 
 * 
 * ------------------------------------------------*/

package diagram;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import data.*;
import javafx.scene.Scene;
import javafx.scene.chart.PieChart;
import javafx.stage.Stage;



public class PieChartDemo1 extends Application {

	/**
	 * Erzeugt einen Datensatz f�r Tortendiagramme
	 * 
	 * @param daten
	 * @return CategoryDataset
	 */
	private ObservableList<PieChart.Data> createPieDataSet(Datenbasis daten) {

		ObservableList<PieChart.Data> list = 
				FXCollections.observableArrayList();
	
		for (int i = 0; i < daten.getBeobachtungsraum().length; i++) {
			list.add(new PieChart.Data(
					daten.getBeobachtungsraum()[i], 
					daten.getMerkmalsauspraegungen()[i].doubleValue()));
		}

		return list;
	}

	
    @Override public void start(Stage stage) {

    	// Daten, die im Diagramm dargestellt werden sollen
 
    	Datenbasis data = new HeizwaermeBau2_2012();

        // Zuordnung der Daten zum Diagramm
       	ObservableList<PieChart.Data> pieChartData = createPieDataSet(data);

    	// Erstellen und Beschriften des Diagramms
       	final PieChart chart = new PieChart(pieChartData);
        chart.setTitle(data.getTopic());
        
        // Rendern des Diagramms
        Scene scene  = new Scene(chart,800,600);
        stage.setTitle("GVI Beispiel: Tortendiagramm");
        stage.setScene(scene);
        stage.show();
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
		
	}
}
