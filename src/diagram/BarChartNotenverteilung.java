/*---------------------------------------------------
 /*---------------------------------------------------
 * Hochschule f�r Technik Stuttgart
 * Fachbereich Vermessung, Informatik und Mathematik
 * Schellingstr. 24
 * D-70174 Stuttgart
 *
 * Volker Coors, 29.8.2017
 * GeoVisualisierung
 * IL3, WS 2017/18
 * 
 * ------------------------------------------------*/

package diagram;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import data.*;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.stage.Stage;


public class BarChartNotenverteilung extends Application {

	/**
	 * Erzeugt einen Datensatz f�r S�ulen- und Balkendiagramme
	 * 
	 * @param daten
	 * @return CategoryDataset
	 */
	private ObservableList<XYChart.Data<String, Number>> createXYDataSet(Datenbasis daten) {

		ObservableList<XYChart.Data<String, Number>> list = 
				FXCollections.observableArrayList();
		
		for (int i = 0; i < daten.getBeobachtungsraum().length; i++) {
			list.add(new XYChart.Data<String, Number>(
					daten.getBeobachtungsraum()[i], 
					daten.getMerkmalsauspraegungen()[i]));
		}

		return list;
	}
	
    @Override public void start(Stage stage) {

    	// Daten, die im Diagramm dargestellt werden sollen
 
    	Datenbasis data = new Notenverteilung();

    	// Erstellen und Beschriften des Diagramms
        final CategoryAxis xAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        
        yAxis.setAutoRanging(false);
        yAxis.setLowerBound(0);
        yAxis.setUpperBound(10);
        yAxis.setTickUnit(1.0);
        yAxis.setMinorTickVisible(false);

        
        final BarChart<String,Number> bc = 
            new BarChart<String,Number>(xAxis,yAxis);
        //bc.setTitle(data.getTopic());
        xAxis.setLabel(data.getNameBeobachtungsraum());       
        yAxis.setLabel(data.getNameMerkmalsauspraegung());
        

        // Zuordnung der Daten zum Diagramm
       	ObservableList<XYChart.Data<String, Number>> barChartData;
    	barChartData = createXYDataSet(data);
        XYChart.Series<String,Number> series1 = 
        		new XYChart.Series<String,Number>(barChartData);
        series1.setName(data.getTopic());

        bc.getData().add(series1);

        // Rendern des Diagramms
        Scene scene  = new Scene(bc,800,600);
        scene.getStylesheets().add("./css/BarChartDemo2.css");
        stage.setTitle("GVI Beispiel: Notenverteilung");
        stage.setScene(scene);
        stage.show();
    }
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
		
	}
}